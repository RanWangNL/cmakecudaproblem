//
// Created by user on 9-6-16.
//

#include "../include/codamod/CudaMatrix.cuh"
#include "../include/codamod/Macro.cuh"
#include <cstdio>
#include <cuda_runtime.h>


template<typename T>
CUDA_HOST CudaMatrix_<T>::CudaMatrix_(int nRow_, int nCol_) {
    const unsigned int  size = sizeof(T)*nRow_*nCol_;
    nRow = nRow_; nCol = nCol_;
    cudaMallocHost((void**)&hostptr, size);
    cudaMalloc((void**)&devptr,size);

    for (int j=0; j != nCol;j++){
        for (int i = 0; i != nRow; i++){
            hostptr[i+j*nRow] = (T)0;
        }
    }
    cudaMemcpy(devptr, hostptr, size, cudaMemcpyHostToDevice);

}

template<typename T>
CUDA_HOST CudaMatrix_<T>::CudaMatrix_(T *data, int nRow_, int nCol_) {
    const unsigned int  size = sizeof(T)*nRow_*nCol_;
    nRow = nRow_; nCol = nCol_;
    cudaMallocHost((void**)&hostptr, size);
    cudaMalloc((void**)&devptr,size);

//    for (long int i=0;i!=nRow*nCol;++i){
//        hostptr[i]=data[i];
//    }
    memcpy(hostptr, data, sizeof(T)*nCol*nRow);

    cudaMemcpy(devptr, hostptr, size, cudaMemcpyHostToDevice);
}

template<typename T>
CUDA_HOST CudaMatrix_<T>::~CudaMatrix_() {
    cudaFreeHost(hostptr);
    cudaFree(devptr);
}

template<typename T>
CUDA_CALLABLE_MEMBER int CudaMatrix_<T>::getNRow(){
    return this->nRow;
}



template<typename T>
CUDA_CALLABLE_MEMBER int CudaMatrix_<T>::getNCol(){
    return this->nCol;
}

template<typename T>
CUDA_HOST void CudaMatrix_<T>::syncDeviceToHost() {
    cudaMemcpy(hostptr, devptr,sizeof(T)*nRow*nCol, cudaMemcpyDeviceToHost);
}

template<typename T>
CUDA_HOST void CudaMatrix_<T>::syncHostToDevice() {
    cudaMemcpy(hostptr, devptr,sizeof(T)*nRow*nCol, cudaMemcpyHostToDevice);
}
template<typename T>
CUDA_HOST void CudaMatrixhelper_<T>::set(CudaMatrix_<T> &other, int i, int j, T value) {
    CHECK_BOUND(i, other.getNRow());
    CHECK_BOUND(i, other.getNCol());
    other.hostptr[i+other.getNRow()*j]=value;
}

template<typename T>
CUDA_HOST T CudaMatrixhelper_<T>::get(CudaMatrix_<T> &other, int i, int j) {
    CHECK_BOUND(i, other.getNRow());
    CHECK_BOUND(i, other.getNCol());
    return (other.hostptr[i+other.getNRow()*j]);
}


void helper(){
    CudaMatrix_<double> doubleProxy= CudaMatrix_<double>(10,10);
    CudaMatrix_<int> intProxy = CudaMatrix_<int>(10,10);
    CudaMatrix_<float> floatProxy = CudaMatrix_<float>(10,10);

    auto  a = CudaMatrixhelper_<double>::get(doubleProxy,0,0);
    CudaMatrixhelper_<double>::set(doubleProxy,0,0,0.0);
    auto  b = CudaMatrixhelper_<float>::get(floatProxy,0,0);
    CudaMatrixhelper_<float>::set(floatProxy,0,0,0.0);
    auto c = CudaMatrixhelper_<int>::get(intProxy,0,0);
    CudaMatrixhelper_<int>::set(intProxy,0,0,0);
}

