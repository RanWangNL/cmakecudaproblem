# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/user/Desktop/CudaTrial/src/cudamod/test/lib/googletest/src/gtest_main.cc" "/home/user/Desktop/CudaTrial/build/cudamod/test/lib/googletest/CMakeFiles/gtest_main.dir/src/gtest_main.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "GTEST_CREATE_SHARED_LIBRARY=1"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/user/Desktop/CudaTrial/build/cudamod/test/lib/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/user/Desktop/CudaTrial/src/cudamod/include/cudamod"
  "/usr/local/cuda-7.5/include"
  "/home/user/Desktop/CudaTrial/src/cudamod/test/lib/googletest/include"
  "/home/user/Desktop/CudaTrial/src/cudamod/test/lib/googletest"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
