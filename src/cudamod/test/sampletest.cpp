//
// Created by user on 8-6-16.
//


#include <gtest/gtest.h>
#include <memory>
#include "../include/codamod/CudaMatrix.cuh"
class CudaMatrix_Test : public ::testing::TestWithParam<int> {
public:
    CudaMatrix_<double> mydouble = CudaMatrix_<double>(100,100);

    //myTestFixture1() {
    // initialization code here
    //}

    //void SetUp( ) {
    // code here will execute just before the test ensues
    //}


    //void TearDown( ) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
    //}

    //~myTestFixture1( )  {
    // cleanup any pending stuff, but no exceptions allowed
    //}

    // put in any custom data members that you need

};

TEST_F(CudaMatrix_Test, TestSetter){
    double b = CudaMatrixhelper_<double>::get(mydouble, 0,0);
    ASSERT_EQ(b,0.0);
}

INSTANTIATE_TEST_CASE_P(Instantiation, CudaMatrix_Test, ::testing::Range(1, 10));
