//
// Created by user on 13-6-16.
//

#include "../include/codamod/CudaRandom.cuh"
#include "../include/codamod/CudaMatrix.cuh"
#include "../include/codamod/Macro.cuh"

template<typename T>
CUDA_HOST void CudaNormalRandom_<T>::create(gen mygen, CudaMatrix_<T>&t ) {
    for (long int i=0;i!=t.getNRow();++i){
        for (long int j=0;j!=t.getNCol();++j){
            CudaMatrixhelper_<T>::set(t, i, j, mygen());
        }
    }
}

template<typename T>
CUDA_HOST CudaNormalRandom_<T>::CudaNormalRandom_(int seed, double mean, double variance):myg(seed), mydist(mean, variance), mygen(myg, mydist) {}

void helperRandom(){
    CudaNormalRandom_<double> mydouble(0,0.0,1.0);
    CudaNormalRandom_<float> myfloat(0,0.0,1.0);
    CudaMatrix_<double> doubleProxy(100,100);
    CudaMatrix_<double> doubleProxy2(100,100);
    CudaMatrix_<float> floatProxy(100,100);
    CudaMatrix_<float> floatProxy2(100,100);
    mydouble.generate(doubleProxy,doubleProxy2);
    myfloat.generate(floatProxy,floatProxy2);
}