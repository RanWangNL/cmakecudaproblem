//
// Created by user on 9-6-16.
//
#ifndef CUDATRIAL_CUDAMATRIX_H
#define CUDATRIAL_CUDAMATRIX_H

#include <cassert>
#include "Macro.cuh"


template<typename T>
class CudaMatrix_ {
private:
    int nRow;
    int nCol;
public:
    T* hostptr;
    T* devptr;


    CUDA_HOST CudaMatrix_(int nRow_, int nCol_);
    CUDA_HOST CudaMatrix_(T* data, int nRow_, int nCol_);
    CUDA_CALLABLE_MEMBER int getNRow();
    CUDA_CALLABLE_MEMBER int getNCol();
    CUDA_HOST void syncDeviceToHost();
    CUDA_HOST void syncHostToDevice();
    CUDA_HOST ~CudaMatrix_();


};
template<typename T>
class CudaMatrixhelper_ {
public:
    CUDA_HOST static void set(CudaMatrix_<T> &other, int i, int j, T value);
    CUDA_HOST static T get(CudaMatrix_<T> &other, int i, int j);
};

#endif //CUDATRIAL_CUDAMATRIX_H

