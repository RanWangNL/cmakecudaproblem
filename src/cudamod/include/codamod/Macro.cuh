

#define __BOUNDCHECK__ 1

#ifdef __BOUNDCHECK__
#define CHECK_BOUND(i, j) (assert((i)<(j)))
#else
#define CHECK_BOUND(i,j)
#endif

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif

#ifdef __CUDACC__
#define CUDA_HOST __host__
#else
#define CUDA_HOST
#endif

#ifdef __CUDACC__
#define CUDA_DEVICE __device__
#else
#define CUDA_DEVICE
#endif